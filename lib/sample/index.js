import data from "./data";
import templateHtml from "./template";
import _ from "underscore";

var template = _.template(templateHtml);

export default = function render(el) {
  if (typeof el !== "string") { throw new TypeError; }
  document.querySelector(el).innerHTML = _(data).map(function(datum) {
    return template(datum);
  }).join('');
};
